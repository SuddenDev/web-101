# Web 101 Landing Page

This website is just for an introduction to learning web development.

Just open the `index.html` inside your favorite browser.

The image was taken by Jack Antal over @ Unsplash.com: https://unsplash.com/photos/s7MaaM4elpQ